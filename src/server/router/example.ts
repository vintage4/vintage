import { createRouter } from "./context";
import { z } from "zod";

export const exampleRouter = createRouter()
  .query("createCategories", {
    async resolve({ ctx }) {
      await ctx.prisma.category.findFirst({
        where: {
          categoryName: "Agriculture"
        },
      });
    },
  });
  // .query("getUser", {
  //   async resolve({ ctx }) {
  //     return await ctx.prisma.user.findMany({
  //       where: {
  //         email: "jayasurya@gmail.com",
  //       },
  //     });
  //   },
  // })
  // .query("getUserPosts", {
  //   async resolve({ ctx }) {
  //     return await ctx.prisma.user.findMany({
  //       where: {
  //         email: "jayasurya@gmail.com",
  //       },
  //       include: {
  //         posts: true,
  //         _count: true,
  //       },
  //     });
  //   },
  // })
  // .mutation("updateUser", {
  //   input: z.object({
  //     email: z.string(),
  //   }),
  //   async resolve({ input, ctx }) {
  //     return await ctx.prisma.user.update({
  //       where: {
  //         email: input.email,
  //       },
  //       data: { role: true },
  //     });
  //   },
  // })
  // .mutation("deletUser", {
  //   async resolve({ ctx }) {
  //     return await ctx.prisma.user.delete({
  //       where: {
  //         email: "jayasurya@gmail.com",
  //       },
  //     });
  //   },
  // });
