import Image from 'next/image';
import { Categories } from './propertyCategories';


export function HeroHeader() {
  return (
    <>
      <div className="bg-gray-100 grid lg:grid-cols-2">
        <div className="mx-auto px-8 py-12 max-w-md sm:max-w-xl lg:px-12 lg:py-28 lg:max-w-full">
          <Image
            src="/HeroImage.jpg"
            alt="header image"
            height={500}
            width={500}
            className="rounded-lg shadow-lg object-center lg:hidden"
          />

          <h1 className="mt-6 text-2xl font-extrabold text-gray-800 antialiased sm:text-4xl">
            Fully featured
            {/* <br className="inline" /> */}
            <span className="text-transparent bg-clip-text bg-gradient-to-r from-green-600 to-blue-900">
              {" "}Property Agent
            </span>
          </h1>

          <p className="mt-2 text-gray-600 antialiased leading-6 sm:text-xl sm:mt-4">
            The bridge that connects potential buyers and sellers by providing
            easy and economic ways of selling, buying and renting properties
            online making it a hastle free and pleasant experience for the
            user.
          </p>
        </div>
        <div className='hidden lg:block relative'>
          <Image
            src="/HeroImage.jpg"
            alt="header image"
            height={500}
            width={500}
            className="absolute inset-0 w-full h-full object-cover object-center"
          />
        </div>
      </div>
      <Categories />
    </>
  );
}
