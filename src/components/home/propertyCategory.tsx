import React, { FC } from 'react';
import Image from 'next/image';

type Category = {
  categoryId: number;
  categoryName: string;
  categoryDescription: string;
  fetures: string[];
  image: string;
};

interface propertyCategoryPropType {
  category: Category;
}
const PropertyCategory: FC<propertyCategoryPropType> = ({ category }) => {
  return (
    <div className='bg-white max-w-sm rounded-lg overflow-hidden shadow-lg'>
      <Image src={category.image}
        className="w-full h-60 object-cover object-center"
        height="500"
        width="500"
        alt={category.categoryName}
      />
      <div className='px-6 py-4'>
        <h1 className='text-lg font-semibold text-gray-800'>{category.categoryName}</h1>
        <div className='flex items-center flex-wrap gap-3 mt-2 mb-4'>
          {
            category.fetures.map((item, index) => {
              return (
                <div key={index} className="bg-blue-100 px-3 py-0.5 rounded-sm">
                  <p className='text-xs font-light tracking-wide text-blue-900'>
                    {item}
                  </p>
                </div>
              );
            })
          }
        </div>

        <p className='my-2 text-gray-500 leading-6 tracking-wide text-sm'>
          {category.categoryDescription}
        </p>
      </div>
    </div>
  );
};

export default PropertyCategory;
