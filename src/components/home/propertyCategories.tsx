import PropertyCategory from "./propertyCategory";

export function Categories() {

  const categoryList = [
    {
      categoryId: 1,
      categoryName: "Agriculture land",
      categoryDescription: "Completely renovated for the season 2020, Arena Verudela Bech Apartments are fully equipped and modernly furnished 4-star self-service apartments located on the Adriatic coastline by one of the most beautiful beaches in Pula.",
      fetures: ['lake view', 'river view', 'mountain view', 'farm house'],
      image: 'https://images.unsplash.com/photo-1652820330042-92a30b18abc5?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=874&q=80'

    },
    {
      categoryId: 2,
      categoryName: "Villas",
      categoryDescription: "Completely renovated for the season 2020, Arena Verudela Bech Apartments are fully equipped and modernly furnished 4-star self-service apartments located on the Adriatic coastline by one of the most beautiful beaches in Pula.",
      fetures: ['with jacuzi', 'wooden interior', 'modern interior', 'tiled roof'],
      image: 'https://images.unsplash.com/photo-1591474200742-8e512e6f98f8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80'

    },
    {
      categoryId: 3,
      categoryName: "Apartments",
      categoryDescription: "Completely renovated for the season 2020, Arena Verudela Bech Apartments are fully equipped and modernly furnished 4-star self-service apartments located on the Adriatic coastline by one of the most beautiful beaches in Pula.",
      fetures: ['lake view', 'river view', 'mountain view'],
      image: 'https://images.unsplash.com/photo-1469022563428-aa04fef9f5a2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=873&q=80'

    },
  ];
  return (
    <div className="max-w-7xl mx-auto px-12 py-8">
      <h2 className="text-xl text-gray-900" > Popular destinations</ h2>
      <p className="mt-2 text-gray-600">A selection of great work-friendly cities with lots to see and explore.</p>
      <div className="mt-6 grid grid-cols-3 gap-8">
        {
          categoryList.map(category => {
            return (

              <PropertyCategory
                key={category.categoryId}
                category={category}
              />

            );
          })
        }
      </div>

    </div >
  );
}
