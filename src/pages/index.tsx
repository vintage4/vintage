import type { NextPage } from "next";
import Head from "next/head";
//import { NextResponse } from "next/server";
import { useEffect, useState } from "react";
import { trpc } from "../utils/trpc";
import { HeroHeader } from "../components/home/heroHeader";

const Home: NextPage = () => {
  //const user = trpc.useQuery(["example.getUser"]);
  //const userPosts = trpc.useQuery(["example.getUserPosts"]);

  //const updateRole = trpc.useMutation("example.updateUser")

  // const [deleteUser, setDeleteUser] = useState<string>();

  //const deleteUser = trpc.useMutation("example.deletUser")

  useEffect(() => {
    (async () => {
      // await hello.mutate();
      // console.log(hello.data?.message);

      // if (hello.data) setDeleteUser(hello.data.message);
      //const getUser = await hello.data
      // console.log(user.data);
      //console.log(userPosts.data);

      //await updateRole.mutate({ email: "jayasurya@gmail.com" })

      //await deleteUser.mutate()

    })();
  }, []);

  return (
    <>
      <Head>
        <title>Home</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <HeroHeader />
    </>
  );
};

export default Home;
