import type { NextPage } from "next";
import Head from "next/head";

const About: NextPage = () => {

    return (
        <>
            <Head>
                <title>About Us</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>
            <body>
                <h1>About page</h1>
            </body>
        </>
    );
};

export default About;
