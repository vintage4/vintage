import React from 'react';
import { Box, FileInput } from '@mantine/core';
import { NativeSelect } from '@mantine/core';
import { useForm } from '@mantine/form';


const AddResource = () => {
    const form = useForm({
        initialValues: {
            email: '',
            termsOfService: false,
        },
    });
    return (
        <Box sx={{ maxWidth: 300 }} mx="auto">
            <form onSubmit={form.onSubmit((values) => console.log(values))}>
                <FileInput
                    placeholder="Pick file"
                    label="Your resume"
                    withAsterisk
                />
                <NativeSelect
                    data={['Category', 'Property']}
                    placeholder="Pick one"
                    label="Select your resource type"
                    withAsterisk
                />
            </form>
        </Box>

    );
};

export default AddResource;
