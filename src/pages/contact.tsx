import type { NextPage } from "next";
import Head from "next/head";

const Contact: NextPage = () => {

    return (
        <>
            <Head>
                <title>Contact</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>
        </>
    );
};

export default Contact;
